resource "aws_autoscaling_schedule" "scale_at_business_hours" {
  scheduled_action_name = "scale-up-during-business-hours"
  min_size              = var.min_instances
  max_size              = var.max_instances
  desired_capacity      = var.desired_instances_during_rush
  recurrence            = "0 9 * * *"

  autoscaling_group_name = aws_autoscaling_group.asg1.name
}

resource "aws_autoscaling_schedule" "scale_at_night" {
  scheduled_action_name = "scale-down-at-night"
  min_size              = var.min_instances
  max_size              = var.max_instances
  desired_capacity      = var.desired_instances_at_night
  recurrence            = "0 17 * * *"

  autoscaling_group_name = aws_autoscaling_group.asg1.name
}
