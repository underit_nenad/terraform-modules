resource "aws_elb" "elb_1" {
  name =        "${var.cluster_name}-elb"
  security_groups = [aws_security_group.elb_sg.id]
  availability_zones = data.aws_availability_zones.available.names

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:${var.server_port}/"
    interval = 30
  }

  listener {
    lb_port      = var.elb_port
    lb_protocol  = "http"
    instance_port = var.server_port
    instance_protocol = "http"
  }
}
