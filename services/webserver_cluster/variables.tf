variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type = number
  default = 8080
}

variable "elb_port" {
  description = "The port on which the load balancer listens"
  type = number
  default = 80
}

variable "cluster_name" {
  description = "The name to use for all the cluster resources"
  type = string
}

variable "instance_type" {
  description = "Defines the type of the EC2 instance"
  type = string
  default = "t2.micro"
}

variable "min_instances" {
  description = "Defines the minimum number of EC2 instances of the ASG"
  type = number
  default = 1
}

variable "max_instances" {
  description = "Defines the maximum amount of EC2 instances in the ASG"
  type = number
  default = 5
}

variable "desired_instances_during_rush" {
  description = "Desired number of instances during business hours"
  type = number
}

variable "desired_instances_at_night" {
  description = "Desired number of instances out of business hours"
  type = number
}
