resource "aws_instance" "food_instance" {
  //ami = "${var.ami}"
  ami = "ami-0c64dd618a49aeee8"
  //instance_type = "${lookup(var.instance_type, terraform.workspace)}"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = [aws_security_group.default_sg.id]

  user_data = <<-EOF
              #!/bin/bash
              echo "Hello, World" > index.html
              nohup busybox httpd -f -p "${var.server_port}" &
              EOF

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "food blog instance"
  }
}
