output "clb_dns_name" {
  value = "aws_elb.elb_1.dns_name"
  description = "The domain name of the load balancer"
}

output "asg_name" {
  value = "aws_autoscaling_group.asg1.name"
  description = "The name of the autoscaling group"
}
