resource "aws_security_group" "default_sg" {
  name = "Default Security Group"

  ingress {
    from_port = var.server_port
    to_port   = var.server_port
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "elb_sg" {
    name = "${var.cluster_name}-elb-sg"

    egress {
      from_port   = 0
      to_port     = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
      from_port = var.elb_port
      to_port = var.elb_port
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
}
