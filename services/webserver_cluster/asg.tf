resource "aws_autoscaling_group" "asg1" {
  launch_configuration = aws_instance.food_instance.id
  availability_zones = data.aws_availability_zones.available.names

  min_size = var.min_instances
  max_size = var.max_instances

  load_balancers = [aws_elb.elb_1.name]
  health_check_type = "ELB"

  tag {
    key     = "Name"
    value   = "${var.cluster_name}-elb"
    propagate_at_launch = true
  }
}
